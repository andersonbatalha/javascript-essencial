/*
Sintaxe:

condicao ? resultado1 : resultado2

Se condicao = verdadeira (retorna resultado1)
Senão (retorna resultado2)

*/

const tenho_dinheiro = true;

let expressao = tenho_dinheiro ? "Comer no restaurante" : "Comer em casa";
console.log(expressao);