let x = Math.round(Math.random() * 100);
let y = Math.round(Math.random() * 100);

console.log(x,y);

// Atribuição
x = y;

console.log(x,y);

// Atribuição e adição
x = x + y;
x += y;

console.log(x,y);

// Atribuição e subtração
y = y - x;
y -= x;

console.log(x,y);

// Atribuição e multiplicação
x = x * y;
x *= y;

console.log(x,y);

// Atribuição e resto
y = y % x;
y %= x;

console.log(x,y);
