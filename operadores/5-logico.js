console.log("AND (&&)"); 
console.log(`true && true = ${true && true}`); // true
console.log(`true && false = ${true && false}`); // false
console.log(`false && true = ${false && true}`); // false
console.log(`false && false = ${false && false}`); // false
console.log(`true && '' = ${true && ''}`); // ''
console.log(`true && ' ' = ${true && ' '}`); // ' '
console.log(`true && 'Gato' = ${true && 'Gato'}`); // string 
console.log(`false && '' = ${false && ''}`); // false
console.log(`false && ' ' = ${false && ' '}`); // false
console.log(`false && 'Gato' = ${false && 'Gato'}`); // false

console.log("\n\nOR (||)");
console.log(`true || true = ${true || true}`); // true
console.log(`true || false = ${true || false}`); // true
console.log(`false || true = ${false|| true}`); // true
console.log(`false || false = ${false || false}`); // false
console.log(`true || '' = ${true || ''}`); // ''
console.log(`true || ' ' = ${true || ' '}`); // ' '
console.log(`true || 'Gato' = ${true || 'Gato'}`); // Gato 
console.log(`false || '' = ${false || ''}`); // false
console.log(`false || ' ' = ${false || ' '}`); // false
console.log(`false || 'Gato' = ${false || 'Gato'}`); // false

console.log("\n\nNOT (!)");
console.log(`!true = ${!true}`); // false
console.log(`!false = ${!false}`); // true
console.log(`!'' = ${!''}`); // true
console.log(`!' ' = ${!' '}`); // false
console.log(`!'Gato' = ${!'Gato'}`); // false
