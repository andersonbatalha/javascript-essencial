console.log("\nMódulo (%) - resto da divisão");

console.log(12 % 6); // 0
console.log(1293 % 26); // 19

console.log("\nIncremento");
x = 5;
x++; // x = 6
console.log(`x++ (x = ${x})`); 
++x; // x = 7
console.log(`++x (x = ${x})`);

x = 38;
x--; 
console.log(`x-- (x = ${x})`);
--x;
console.log(`--x (x = ${x})`);

console.log("\nNegação e Adição");
console.log(-(true));
console.log(-(false));

console.log("\nExponenciação");
console.log(`10 ^ 3 = ${10 ** 3}` );

console.log("\nDivisão");
console.log(`100 / 25 = ${100 / 25}` );
