/*

Spread (...)

*/ 

var inicio = [0,1,2,3,4];

var numeros = [...inicio, 5,6,7,8,9] 
console.log(numeros); // [0,1,2,3,4,5,6,7,8,9];

// funções

function fn (x,y,z) {
    console.log(`${x} / ${y} / ${z}`);
}

var args = ["A", "B", "C"];

fn(...args);

