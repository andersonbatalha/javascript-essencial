console.log("Operador in");
var carros = new Array(["Gol", "Celta", "Prisma", "Sandero", "Logan", "EcoSport", "HB20", "Sportage", "Siena", "Ranger", "Renegade", "ix35", "Corolla", "Corsa"]);
var string = new String("Texto de exemplo");

// arrays
/** Usar o índice, não o valor */
console.log(`'Gol' in carros = ${'Gol' in carros}`); // false
console.log(`0 in carros = ${0 in carros}`); // true
console.log(`20 in carros = ${20 in carros}`); // false

console.log(`\n"PI" in Math = ${"PI" in Math}`); // true
console.log(`"round" in Math = ${"round" in Math}`); // true
console.log(`"length" in string = ${"length" in string}`); // true
console.log(`"teste" in string = ${"teste" in string}`); // false

// objetos
var carro = {
    'marca': 'Chevrolet',
    'modelo': "Prisma",
    'ano': 2012,
    'motor': '1.4',
}

console.log(`marca in carro = ${'marca' in carro}`); // true
console.log(`modelo in carro = ${'modelo' in carro}`); // true
console.log(`opcionais in carro = ${'opcionais' in carro}`); // false
console.log(`versao in carro = ${'versao' in carro}`); // false
console.log(`ano in carro = ${'ano' in carro}`); // true

/*

instanceof

Ex: objeto instanceof tipoObjeto

*/

console.log(`\ninstanceof`);
var now = new Date();
console.log(`Data atual: ${now}`);
console.log(`now instanceof Date = ${now instanceof Date}`); // True
console.log(`now instanceof String = ${now instanceof String}`); // False
console.log(`now instanceof Array = ${now instanceof Array}`); // False
console.log(`now instanceof Object = ${now instanceof Object}`); // True


