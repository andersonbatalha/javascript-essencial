console.log("\n\nMaior que (>)");
console.log(`3 > 4 = ${3 > 4}`); // false

console.log("\n\nMaior ou igual (>=)");
console.log(`10 >= 10 = ${10 >= 10}`); // true

console.log("\n\nMenor que (<)");
console.log(`125 + 8765 < 75453 = ${125 + 8765 < 75453}`); // true

console.log("\n\nMenor ou igual (<=)");
console.log(`56423 + 86931 <= 75453 * 43 = ${56423 + 86931 <= 75453 * 43}`); // true

console.log("\n\nIgual (==): comparação entre valor");
console.log(`'3' == 3 ${'3' == 3}`); // true
console.log(`'Anderson' == 'anderson' ${'Anderson' == 'anderson'}`); // false

console.log("\n\nEstritamente igual (===): comparação de tipo e valor");
console.log(`'3' === 3 ${'3' === 3}`); // false
console.log(`21 === 43 ${21 === 43}`); // false
