### 2. Conceitos

* Linguagem interpreta
* Tipagem fraca e dinâmica
* Funções de primeira classe e ordem maior    
    * A função pode ser atribuída a uma variável ou estrutura de dados (object)

* Closure/escopo léxico
    * Tipos de escopo 

        a. Escopo de função

        b. Escopo global
        
        c. Escopo de bloco
    
* Currying
    * Consiste em modificar uma função com vários parâmetros em uma com apenas um
    * Para cada parâmetro adicional, cria uma nova função
    * Evita repetição de código

* Hoisting
    * Hoisting de função ou variável
    
    * Uma boa prática é sempre declarar a função ou variável antes de seu uso

* Imutabilidade
    * Quando uma variável é criada, seu valor é imutável
     
    #### 2.1. Tipos de variáveis
    
    - Definição de variáveis

        ```
        var nome_variavel = valor;
        let nome_variavel = valor;
        const nome_variavel = valor;
        ```
    - Escopo

        a. Escopo de bloco: variáveis criadas dentro de estruturas condicionais (if, else, case) ou de repetição (while, for)

        b. Escopo de função: variáveis criadas dentro de funções
    
        c. Escopo global: variáveis criadas fora do escopo de bloco ou função
    
    - ```var```, ```let``` e ```const```

        - ```var```: aceita apenas escopo de função ou escopo global
        - ```let```: respeita escopo de bloco
        - ```const```: respeita escopo de bloco

            - Variáveis criadas com ```const``` não podem ter seu valor. No caso de objetos, podem ter apenas suas propriedades modificadas. Em listas, podem ser feitas operações de exclusão e inserção
