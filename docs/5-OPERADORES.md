### 5. Operadores
    
* Aritméticos
    * Módulo (%) - resto da divisão
        ```
        console.log(12 % 6); // 0
        console.log(1293 % 26); // 19
        ```
    * Incremento e decremento

        ```
        x = 5;
        x++; // x = 6
        ++x; // x = 7

        x = 38;
        x--; 
        --x;
        ```

    * Negação e Adição
        ```
        console.log(-(true));
        console.log(-(false));
        ```

    * Exponenciação
        ```
        console.log(10 ** 3); // 1000
        ```
    * Divisão

        ```
        console.log(100 / 25); // 4
        ```

* Atribuição
    * Atribuição
        ```
        x = y;
        ```
    * Atribuição e adição
        ```
        x = x + y;
        x += y;
        ```
    * Atribuição e subtração
        ```
        y = y - x;
        y -= x;
        ```
    
    * Atribuição e multiplicação
        ```
        x = x * y;
        x *= y;
        ```
    * Atribuição e resto
        ```
        y = y % x;
        y %= x;
        ```

* Comparação
    * Maior que (>)
        ```
        console.log(3 > 4); // false
        ```

    * Maior ou igual (>=)
        ```
        console.log(10 >= 10); // true
        ```

    * Menor que (<)
        ```
        console.log(125 + 8765 < 75453); // true
        ```

    * Menor ou igual (<=)
        ```
        console.log(56423 + 86931 <= 75453 * 43); // true
        ```

    * Igual (==): comparação entre valor
        ```
        console.log(`'3' == 3`); // true
        console.log('Anderson' == 'anderson'); // false
        ```

    * Estritamente igual (===): comparação de tipo e valor
        ```
        console.log('3' === 3); // false
        console.log(21 === 43); // false
        ```

* Condicional
    * Sintaxe:
        ```
        condicao ? resultado1 : resultado2
        ```
        * Se condicao = verdadeira (retorna resultado1)
        Senão (retorna resultado2)
    * Exemplo
        ```
        const tenho_dinheiro = true;

        let expressao = tenho_dinheiro ? "Comer no restaurante" : "Comer em casa";
        console.log(expressao);
        ```

* Lógico

    * AND (&&) 
    ```
    console.log(true && true); // true
    console.log(true && false); // false
    console.log(false && true); // false
    console.log(false && false); // false
    console.log(true && ''); // ''
    console.log(true && ' '); // ' '
    console.log(true && 'Gato'); // string 
    console.log(false && ''); // false
    console.log(false && ' '); // false
    console.log(false && 'Gato'); // false
    ```

    * OR (||)

    ```
    console.log(true || true); // true
    console.log(true || false); // true
    console.log(false|| true); // true
    console.log(false || false); // false
    console.log(true || ''); // ''
    console.log(true || ' '); // ' '
    console.log(true || 'Gato'); // Gato 
    console.log(false || ''); // false
    console.log(false || ' '); // false
    console.log(false || 'Gato'); // false
    ```

    * NOT (!);

    ```
    console.log(!true}); // false
    console.log(!false}); // true
    console.log(!''); // true
    console.log(!' '}); // false
    console.log(!'Gato'); // false
    ```

* Spread

    ```
    var inicio = [0,1,2,3,4];

    var numeros = [...inicio, 5,6,7,8,9] 
    console.log(numeros); // [0,1,2,3,4,5,6,7,8,9];
    ```

    ```
    // funções

    function fn (x,y,z) {
        console.log(`${x} / ${y} / ${z);
    }

    var args = ["A", "B", "C"];

    fn(...args); /* A / B / C */
    ```

* Operadores unários

    * delete

    ```
    console.log("deletar algo");

    var lista = [0,1,2,3];
    console.log(lista); // [0,1,2,3]
    delete lista[3];
    console.log(lista); // [0,1,2]
    ```

    * typeof
    ```
    var x = 0;
    var texto = "Texto";
    var lista =  [-12,34,65,-6];

    console.log(x, typeof x); // number
    console.log(texto, typeof texto); // String
    console.log(lista, typeof lista); // Object
    ```

* Operadores binários

    * in
        
        * arrays 
        ```
        var carros = new Array(["Gol", "Celta", "Prisma", "Sandero", "Logan", "EcoSport", "HB20", "Sportage", "Siena", "Ranger", "Renegade", "ix35", "Corolla", "Corsa"]);
        var string = new String("Texto de exemplo");

        /** Usar o índice, não o valor */
        console.log('Gol' in carros); // false
        console.log(0 in carros); // true
        console.log(20 in carros); // false

        console.log("PI" in Math); // true
        console.log("round" in Math); // true
        console.log("length" in string); // true
        console.log("teste" in string); // false
        ```

        * objetos
        ```
        // objetos
        var carro = {
            'marca': 'Chevrolet',
            'modelo': "Prisma",
            'ano': 2012,
            'motor': '1.4',
        }

        console.log('marca' in carro); // true
        console.log('modelo' in carro); // true
        console.log('opcionais' in carro); // false
        console.log('versao' in carro); // false
        console.log('ano' in carro); // true
        ```

    * instanceof
        * Sintaxe: ```objeto instanceof tipoObjeto```
        ```
        var now = new Date();
        console.log(`Data atual: ${now);
        console.log(now instanceof Date); // True
        console.log(now instanceof String); // False
        console.log(now instanceof Array); // False
        console.log(now instanceof Object); // True
        ```
