### 1. História

* Lançado em setembro de 1995, juntamente com o navegador Netscape
* Criado por Brendan Eich
* ECMAScript: especificação do JavaScript submetida a ECMA International (associação responsável por padronizar sistemas de informação)
    * Especificações ECMA-262 e ECMA-402
* Fluxo da proposta

    As alterações no JavaScript devem passar por uma série de etapas até sua formalização, descritas abaixo:

    1. stage 0 (strawman/rascunho): submissão de um formulário por um membro do TC39 ou contribuidor
    2. stage 1 (proposal/proposta): definição de um representante (champion) da proposta; descrição do problema a ser resolvido; exemplos; discussão dos aspectos principais do algoritmo
    3. stage 2 (draft/esboço): descrição completa da sintaxe e semântica; deve ter duas implementações, em uma delas pode ser usado um transpilador (ex.:Babbel)
    4. stage 3 (candidate/candidata): finalização e revisão da especificação; feedback dos usuários;
    5. stage 4 (finished/finalizada): proposta pronta para entrar formalmente na especificação do JavaScript; deve ter passado nos testes de aceitação e cumprir todos os requisitos. 

* [Repositório do TC39](https://github.com/tc39)
* [Propostas do TC39](https://github.com/tc39/proposals)
