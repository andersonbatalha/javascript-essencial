### 7. Design patterns

#### 1. Definição

* Design patterns/padrões de projeto são soluções generalistas para problemas recorrentes durante o desenvolvimento de software
* Definição em alto nível de como um problema comum pode ser solucionado

#### 2. Publicações e palestras importantes em Design Patterns

* A Pattern Language (1978)  
    * Christopher Alexander, Sara Ishikawa, Murray Silverstein
    * Descreve 253 tipos de problemas/desafios de projetos
    * Explica qual o formato de um pattern 
        * Nome do pattern 
        * Exemplo de utilização
        * Contexto em que pode ser utilizado
        * Que tipo de problema o pattern resolve
        * Como a solução proposta pelo pattern resolve o problema

* Using Pattern Languages for Object-Oriented Programs (1987)
    * Kent Beck e Ward Cunningham
    * 5 tipos de padrões de projeto

* Design Patterns: Elements of Reusable Object-Oriented Software (1994)
    * Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides (Gang of Four - GoF)

#### 3. Tipos

1. Padrões de criação: abstraem e/ou adiam o processo de criação dos objetos. Eles ajudam a tornar um sistema independente de como seus objetos são criados, compostos e representados
    * Abstract Factory
    * Builder
    * Factory Method
    * Prototype 
    * Singleton

2. Padrões estruturais: se preocupam com a forma como classes e objetos são compostos para formar estruturas maiores
    * Adapter
    * Bridge
    * Composite
    * Decorator
    * Facade
    * Business Delegate
    * Flyweight
    * Proxy

3. Padrões comportamentais: se concentram nos algoritmos e atribuições de responsabilidades entre os objetos. Eles não descrevem apenas padrões de objetos ou classes, mas também os padrões de comunicação entre os objetos.

    * Chain of Responsibility
    * Command
    * Interpreter
    * Iterator
    * Mediator
    * Observer
    * State
    * Strategy
    * Template Method
    * Visitor

#### 4. Quais os mais utilizados

1. Factory: todas as funções que retornam um objeto, sem a necessidade de chamá-las com o ```new```, são consideradas funções Factory.
    ```
    function PessoaFactory1() {
        return {
            'nome': "Anderson",
            'sobrenome': "Pontes",
        }
    }

    const p1 = PessoaFactory1();
    console.log(p1);

    function PessoaFactory2(novo_nome, novo_sobrenome) {
        return {
            novo_nome,
            novo_sobrenome,
        }
    }

    const p2 = PessoaFactory2("Carlos", "Pereira da Costa");
    console.log(p2);

    console.log("\n\n");
    function PessoaFactory3(properties) {
        return {
            'nome': "Anderson",
            'sobrenome': "Pontes",
            ...properties,
        }
    }

    const p3 = PessoaFactory3({'idade': 25, 'profissao': "Programador"});
    console.log(p3);
    ```



2. Singleton: o objetivo deste pattern é criar uma única instância de uma função construtora e retorná-la toda vez que for necessário utilizá-la.
    ```
    function Pessoa() {
        if (!Pessoa.instance) {
            Pessoa.instance = this;
        }
        return Pessoa.instance;
    }

    const p1 = Pessoa.call({'nome':"Anderson"});
    const p2 = Pessoa.call({'nome':"Carlos"});

    console.log(p1);
    console.log(p2); // o valor de 'name' não se altera, pois foi instanciado primeiro em p1
    ```

3. Decorator: uma função decorator recebe outra função como parâmetro e estende seu comportamento sem modificá-la explicitamente.
    ```
    let loggedIn = false;

    function callIfAuthenticated(fn) {
        return !!loggedIn && fn;
    }

    function soma(x,y) {
        return x+y;
    }

    let funcao = soma(13,45);
    console.log(callIfAuthenticated(funcao));
    loggedIn = true;
    console.log(callIfAuthenticated(funcao));
    console.log(callIfAuthenticated(soma(27,4)));

    ```

4. Observer: A instância (subscriber) mantém uma coleção de objetos (observers) e notifica todos eles quando ocorrem mudanças no estado.
    ```
    class Observer {
        constructor () {
            this.observables = [];
        }

        subscribe(f) {
            this.observables.push(f);
        }

        unsubscribe(f) {
            this.observables = this.observables.filter(value => value !== f);
        }

        notify (data) {
            this.observables.forEach(obs => {
                console.log(obs, data);
            });
        }
    }

    const O = new Observer();

    let logFn1 = () => console.log("Função 1");
    let logFn2 = () => console.log("Função 2");
    let logFn3 = () => console.log("Função 3");

    console.log(O.observables);

    O.subscribe(logFn1);
    O.subscribe(logFn2);
    O.subscribe(logFn3);

    console.log("\n");
    console.log(O.observables);

    console.log("\n");
    O.notify("Notified!");

    O.unsubscribe(logFn2);

    console.log("\n");
    console.log(O.observables);

    console.log("\n");
    O.notify("Notified!");

    ```

5. Module: é um pattern que possibilita organizarmos melhor nosso código, sem a necessidade de expor variáveis globais.
    * Arquivo module
    ```
    let name = "Anderson";

    function getName() {
        return name;
    }

    function setName(newName) {
        name = newName;
    }

    module.exports = {
        getName, 
        setName,
    };
    ```

    * Importando em outro arquivo

    ```
    const {getName, setName} = require('./5-module.js');

    console.log(getName());
    setName("Marcos");
    console.log(getName());
    ```