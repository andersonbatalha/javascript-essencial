### 8. Operações com arrays

#### 1 - Criação de array

Existem formas diferentes para criação de um array em JavaScript:

* Definindo os elementos separados por vírgulas, e entre colchetes

```
let myList = [0,1,2,3,4,5,6,7,8,9];
```

* Utilizando a classe `Array`

```
let persons = new Array("Carlos", "Maria", "André", "Luisa");
```

* `Array.of()`
```
let frutas = Array.of("Melancia", "Uva", "Morango", "Laranja");
```

* `Array.from()`: mapeia um NodeList para um objeto do tipo Array
```
const divs = document.querySelectorAll('div'); 
console.log(typeof divs); // NodeList
const divsList = Array.from(divs); 
console.log(typeof divsList); // Array
```

#### 2 - Inserção e remoção

* Array.push(): insere no fim do array e retorna o número de itens
```
const paises = ["Brasil", "Uruguai", "Argentina"]

paises.push("Estados Unidos"); // retorna 5
console.log(paises); // ["Brasil", "Uruguai", "Argentina", "Estados Unidos"]
paises.push("Rússia", "México"); // retorna 7
console.log(paises); // ["Brasil", "Uruguai", "Argentina", "Estados Unidos", "Rússia", "México"]
```

* Array.pop(): remove no fim do array e retorna o elemento removido 
```
paises.pop(); // "México"
console.log(paises); // ["Brasil", "Uruguai", "Argentina", "Estados Unidos", "Rússia"]


paises.pop(); // "Rússia"
console.log(paises); // ["Brasil", "Uruguai", "Argentina", "Estados Unidos"]
```

* Array.shift(): remove no início do array e retorna o elemento removido
```
paises.shift(); // "Brasil"
console.log(paises); // ["Uruguai", "Argentina", "Estados Unidos"]
```

* Array.unshift(): insere no início do array e retorna o número de itens
```
paises.unshift("Panamá"); // 4
console.log(paises); // ["Panamá", "Uruguai", "Argentina", "Estados Unidos"]

paises.unshift("Jamaica", "El Salvador"); // 6
console.log(paises); // ["Jamaica", "El Salvador", "Panamá", "Uruguai", "Argentina", "Estados Unidos"]
```

* Array.concat(): une os elementos de dois ou mais arrays"
```
let capitais = ["Brasília", "Buenos Aires", "Washington D.C"]
let paises_cidades = paises.concat(capitais);
console.log(paises_cidades); 
/* ["Jamaica", "El Salvador", "Panamá", 
"Uruguai", "Argentina", "Estados Unidos", 
"Brasília", "Buenos Aires", "Washington D.C"] */
```

* Array.slice(): retorna apenas uma parte de um array

```
paises.slice(1,4); // ["El Salvador", "Panamá", 
"Uruguai"]
paises.slice(3); // ["Uruguai", "Argentina", "Estados Unidos", "Brasília", "Buenos Aires", "Washington D.C"]
paises.slice(-2); // ["Buenos Aires", "Washington D.C"]
```
* Array.splice(): permite remover parte de uma lista, ao mesmo tempo em que adiciona elementos
    * Parâmetros: 
        - posição onde começam a ser removidos 
        - quantidade de elementos
        - novo elemento a ser inserido

```/** Iniciando da posição 2, remove 3 elementos e coloca no ] o item 'São Paulo' */
paises_cidades.splice(2,3, "São Paulo"); 
console.log(paises_cidades);
/* ["Jamaica", "El Salvador", "São Paulo", "Estados Unidos", "Brasília", "Buenos Aires", "Washington D.C"] */
```

