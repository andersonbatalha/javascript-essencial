### 4. Functions

* Existem duas formas de declarar uma função

```

    function nome_funcao(parametro1, parametro2, ..., parametro n) {
    // instruções
    }


    const minha_funcao = () => {
        // instruções
    }

```
