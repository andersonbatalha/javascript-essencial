### 6. Orientação a objetos

#### 1. Herança

* Baseada em protótipos
* ```prototype```: armazena as definições do objeto
* ```__proto__```: aponta para prototype -> constructor

* Exemplo

```
const myText = "Texto";
console.log(myText.split());

console.log(myText.__proto__);

const myText2 = String("Outro texto");
console.log(String.prototype.split);

// myText2.constructor == String;
// myText2.__proto__ == String.prototype;

```

#### 2. Classes

* Definida a partir da versão ES6
* simplificação da herança de protótipos
* uso da palavra ```class```
* Exemplo

```
class Animal {
    constructor(qtdePatas) {
        this.qtdePatas = qtdePatas;
    }
    movimentar() {
        console.log("Andando...");
    }
}
```

#### 3. Modificadores de acesso

* O JavaScript não possui modificadores de acesso
* Define o que é privado/público
* Exemplo

```

class Pessoa {
    #name = ''; 
    constructor(nome) {
        this.#name = nome;
    }

    getNome() {
        return this.#name;
    }

    setNome(novo_nome) {
        this.#name = novo_nome;
    }
}

```

#### 4. Encapsulamento

* Permite ocultar detalhes do funcionamento interno de uma classe

```

class Pessoa {
    #name = '';
    constructor(nome) {
        this.#name = nome;
    }

    get nome() {
        return this.#name;
    }

    set nome(novo_nome) {
        this.#name = novo_nome;
    }
}

```
* No exemplo acima, para acessar os métodos ```get``` e ```set```, basta alterar o valor da variável ```nome```.

#### 5. Static

* Permite acessar métodos/atributos sem instanciar uma classe


```

class PersonClass {
    static falar(frase) {
        return frase;
    }
}

console.log(PersonClass.falar("Olá, e aí galera?"));

```