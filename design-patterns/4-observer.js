class Observer {
    constructor () {
        this.observables = [];
    }

    subscribe(f) {
        this.observables.push(f);
    }

    unsubscribe(f) {
        this.observables = this.observables.filter(value => value !== f);
    }

    notify (data) {
        this.observables.forEach(obs => {
            console.log(obs, data);
        });
    }
}

const O = new Observer();

let logFn1 = () => console.log("Função 1");
let logFn2 = () => console.log("Função 2");
let logFn3 = () => console.log("Função 3");

console.log(O.observables);

O.subscribe(logFn1);
O.subscribe(logFn2);
O.subscribe(logFn3);

console.log("\n");
console.log(O.observables);

console.log("\n");
O.notify("Notified!");

O.unsubscribe(logFn2);

console.log("\n");
console.log(O.observables);

console.log("\n");
O.notify("Notified!");
