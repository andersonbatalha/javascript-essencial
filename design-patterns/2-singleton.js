function Pessoa() {
    if (!Pessoa.instance) {
        Pessoa.instance = this;
    }
    return Pessoa.instance;
}

const p1 = Pessoa.call({'nome':"Anderson"});
const p2 = Pessoa.call({'nome':"Carlos"});

console.log(p1);
console.log(p2); // o valor de 'name' não se altera, pois foi instanciado primeiro em p1