let loggedIn = false;

function callIfAuthenticated(fn) {
    return !!loggedIn && fn;
}

function soma(x,y) {
    return x+y;
}

let funcao = soma(13,45);
console.log(callIfAuthenticated(funcao));
loggedIn = true;
console.log(callIfAuthenticated(funcao));
console.log(callIfAuthenticated(soma(27,4)));
