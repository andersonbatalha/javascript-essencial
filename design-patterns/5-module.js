let name = "Anderson";

function getName() {
    return name;
}

function setName(newName) {
    name = newName;
}

module.exports = {
    getName, 
    setName,
};