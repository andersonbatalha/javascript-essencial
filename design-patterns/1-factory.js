function Pessoa(nome) {
    this.nome = nome;
}

// não é Factory
const p = new Pessoa("Anderson"); 
console.log(p);

console.log("\n\n");
function PessoaFactory() {
    return {
        'nome': "Anderson",
        'sobrenome': "Pontes",
    }
}

const p1 = PessoaFactory();
console.log(p1);

console.log("\n\n");
function PessoaFactory2(novo_nome, novo_sobrenome) {
    return {
        novo_nome,
        novo_sobrenome,
    }
}

const p2 = PessoaFactory2("Carlos", "Pereira da Costa");
console.log(p2);

console.log("\n\n");
function PessoaFactory3(properties) {
    return {
        'nome': "Anderson",
        'sobrenome': "Pontes",
        ...properties,
    }
}

const p3 = PessoaFactory3({'idade': 25, 'profissao': "Programador"});
console.log(p3);