// Declaração de funções

function teste() {
    console.log("Olá mundo! (função normal)");
}

teste();

// Arrow function
let msg = () => "Olá mundo! (com arrow function)";
msg.outraMensagem = "Testando...";
console.log(msg());
console.log(`Propriedade da função msg: ${msg.outraMensagem}`); // functions também podem receber propriedades


let soma = (x,y) => x + y; // quando o retorno da função é apenas uma instrução, o 'return' não é necessário
console.log(soma(1,1));

let executar = (funcao) => funcao;
console.log(executar(soma(212, 82)));

const podeDirigir = (idade) => {
    if (idade >= 18) {
        return "Pode dirigir";
    }
    else {
        return "Não pode dirigir";
    }
}

console.log(podeDirigir(20));

const controleExec = funcParam => sim => {
    if (sim) {
        return funcParam();
    }
    else {
        console.log("Não executada a função");
    }
}

const func1 = controleExec(teste);
func1();
func1(true);


