// Objetos

// criar objeto
let pais = {
    'nome': "Brasil",
    'capital': "Brasília",
    'moeda': 'Real',
    'idioma': "Português",
};

console.log(pais);

// acessar propriedades
let nomePais = pais.nome;
console.log(`Acessando a propriedade 'nome' do objeto 'pais': ${nomePais}`);

let capitalPais = pais['capital'];
console.log(`Acessando a propriedade 'capital' do objeto 'pais': ${capitalPais}`);

// remover proprieades

delete pais.idioma;
console.log(`Após a remoção:`, pais);

// funções: keys, values, entries, assign, freeze, seal

// Object.keys() = retorna as chaves do objeto
let chaves = Object.keys(pais);
console.log(`Chaves do objeto:`, chaves);

// Object.values() = retorna os valores do objeto
let valores = Object.values(pais);
console.log(`Valores do objeto:`, valores);

// Object.entries() = retorna um array onde cada item é composto por outro array com chave e valor
let entries = Object.entries(pais);
console.log(`Usando a propriedade Object.entries():`, entries);

// Object.assign() = permite adicionar uma propriedade a um objeto existente

/* FORMA INCORRETA

Object.assign(pais, {'idioma': "Português", 'populacao': 200000000});

*/

// correto: criar um novo objeto, adicionando as propriedades do objeto anterior
/*
FORMA CORRETA

let novoObjeto = Object.assign({}, pais, {'idioma': "Português", 'populacao': 200000000}); 
console.log(`Criando uma 'cópia' do objeto e adicionando propriedades:`, novoObjeto);
console.log(`Original:`, pais);

*/

// Object.freeze() = impede mudanças nas propriedades do objeto, criação e remoção de propriedades

Object.freeze(pais);
pais['maiorCidade'] = "São Paulo"; 
delete pais.capital;

console.log(`Object.freeze() impede a alteração do objeto`, pais); // não alterou nada

// Object.seal() = impede a criação e remoção, mas permite alterar os valores das propriedades

Object.seal(pais);
pais['maiorCidade'] = "São Paulo"; 
pais.nome="França";
pais.capital = "Paris";
pais.idioma = "Francês";
pais.moeda = "Euro";

console.log(`Object.seal() impede a alteração do objeto`, pais); // altera apenas valores das propriedades

