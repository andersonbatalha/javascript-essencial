// symbols são únicas
let symbol1 = Symbol("nome");
let symbol2 = Symbol("nome");

console.log(`symbol1 e symbol2 são iguais: ${symbol1 === symbol2}`);

let idade = Symbol("idade"); 
let obj = {
    'nome': "Anderson",
    'sobrenome': "Pontes Batalha",
    [idade]: 25, // quando o nome da propriedade for uma variável aparece entre colchetes
}

// O uso de symbols permite ocultar propriedades de um objeto 
for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
        console.log(`Chave ${key} / Valor ${obj[key]}`);
    }
}

// Object.getOwnPropertySymbols(): retorna as variáveis do tipo symbol
console.log(Object.getOwnPropertySymbols(obj));

// Reflect.ownKeys(): retorna todos as chaves do objeto
console.log(Reflect.ownKeys(obj));
