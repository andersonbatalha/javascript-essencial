let string = "Meu primeiro texto";

// Operações com strings

// 1 - Número de caracteres da string
let tamanho = string.length;
console.log(`Número de caracteres da string: ${tamanho}`);


// 2 - Substituir parte de uma string
let novaString = string.replace('primeiro', 'último');
console.log(`Nova string após a função replace: ${novaString}`);

// 3 - Separar partes de uma string
let palavras = string.split(" ");
console.log(`Separando as palavras do texto pelo delimitador de espaço: ${palavras}`);

// 4 - Retorna parte de uma string
let pedacoString = string.substring(4, 12);
console.log(`Pedaço de uma string: ${pedacoString}`);

// 5 - slice
let parte = string.slice(0,4);
let comeco = string.slice(0,9);
let metade = string.slice(string.length / 2);
let final = string.slice(-3); // valores negativos retorna os últimos caracteres da string
console.log(`Método slice: ${parte}`);
console.log(`Oito primeiros caracteres: ${comeco}`);
console.log(`Metade da string: ${metade}`);
console.log(`Últimos 3 caracteres: ${final}`);

