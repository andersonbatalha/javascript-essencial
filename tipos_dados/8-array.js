const genero = {
    MASC: Symbol("M"),
    FEM: Symbol("F"),
}

const pessoas = [
    {
        'nome': "Anderson",
        'idade': 25,
        'genero': genero.MASC,
    },
    {
        'nome': "Renata",
        'idade': 42,
        'genero': genero.FEM,
    },
    {
        'nome': "Marcos",
        'idade': 57,
        'genero': genero.MASC,
    },
    {
        'nome': "Luisa",
        'idade': 21,
        'genero': genero.FEM,
    },
]

//length: retorna o número de elementos do array
console.log(`\nO array tem ${pessoas.length} elementos.`);

//isArray
console.log(`\nisArray(): ${Array.isArray(pessoas)}`);

//array.forEach(element => {});
console.log(`\nIterar sobre os elementos de um array`);
pessoas.forEach(pessoa => {
    console.log(pessoa.nome);
})

// filter
console.log(`\n\nFILTER`);
console.log(`\nPessoas do sexo masculino`);
let homens = pessoas.filter(pessoa => pessoa.genero === genero.MASC);
console.log(homens);

console.log(`\nPessoas do sexo feminino com mais de 30 anos`);
let mulheresAcimaDe30 = pessoas.filter(pessoa => pessoa.genero === genero.FEM && pessoa.idade >= 30);
console.log(mulheresAcimaDe30);


// map
console.log(`\n\nMAP`);
let novoArray = pessoas.map(pessoa => {
                    pessoa.local_nascimento = "São Paulo";
                    return pessoa;
                });

console.log(novoArray);

// reduce
console.log(`\n\nREDUCE\nSoma das idades`);
let somaIdades = pessoas.reduce(
    (idade, pessoa) => {
        idade += pessoa.idade;
        return idade;
    }, 
    0);
console.log(somaIdades);