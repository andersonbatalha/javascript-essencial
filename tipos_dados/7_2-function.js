(() => {
    this.name = "arrow function"; // (1) Escopo da arrow function

    const getNameArrowFn = () => this.name;

    function getName() { // (2) Variável 'name' do objeto
        return this.name;
    }

    const user = {
        name: "Nome do objeto",
        getNameArrowFn,
        getName,
    }

    console.log(user.getNameArrowFn());  // (1) Escopo da arrow function
    console.log(user.getName()); // (2) Variável 'name' do objeto

})();