const meuNumero = 475.23512;

const strNumero = meuNumero.toString();
console.log(`Conversão para string: ${strNumero} (${typeof strNumero})`);

const valor = meuNumero.toFixed(2);
console.log(`Delimita as casas decimais: ${valor}`);

const parse_int_exemplo = parseInt('65312.4321');
console.log(`Converte string para inteiro: ${parse_int_exemplo}`);

const parse_float_exemplo = parseFloat('65312.4321');
console.log(`Converte string para float: ${parse_float_exemplo}`);

const parse_float_exemplo2 = parseFloat('65312.4321').toFixed(2);
console.log(`Converte string para float (delimitando número de casas decimais): ${parse_float_exemplo2}`);
