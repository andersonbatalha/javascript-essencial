// Inserção/remoção no fim do array

const paises = ["Brasil", "Uruguai", "Argentina", ]
console.log(paises);

console.log("\nArray.push(): insere no fim do array e retorna o número de itens"); 
console.log(paises.push("Estados Unidos"));
console.log(paises);
console.log(paises.push("Rússia", "México"));
console.log(paises);


console.log("\nArray.pop(): remove no fim do array e retorna o elemento removido"); 
console.log(paises.pop());
console.log(paises.pop());
console.log(paises);

// Inserção/remoção no início do array
console.log("\nArray.shift(): remove no início do array e retorna o elemento removido");
console.log(paises.shift());
console.log(paises);

console.log("\nArray.unshift(): insere no início do array e retorna o número de itens");
console.log(paises.unshift("Panamá"));
console.log(paises);
console.log(paises.unshift("Jamaica", "El Salvador"));
console.log(paises);

console.log("\nArray.concat(): une os elementos de dois ou mais arrays");
let capitais = ["Brasília", "Buenos Aires", "Washington D.C"]
let paises_cidades = paises.concat(capitais);
console.log(paises_cidades);

console.log("\nArray.slice(): retorna apenas uma parte de um array");
console.log(`paises.slice(1,4): retorna os elementos das posições 1 até 3, excluindo o 4 -> ${paises.slice(1,4)}`);
console.log(`paises.slice(3): retorna os elementos da posição 3 até o fim da lista -> ${paises.slice(3)}`);
console.log(`paises.slice(-2):  retorna os dois últimos elementos da lista -> ${paises.slice(-2)}`);

console.log("\nArray.splice(): permite remover parte de uma lista, ao mesmo tempo em que adiciona elementos");
console.log("\nParâmetros: posição onde começam a ser removidos, quantidade de elementos, novo elemento a ser inserido");

console.log(paises_cidades);
/** Iniciando da posição 2, remove 3 elementos e coloca no ] o item 'São Paulo' */
paises_cidades.splice(2,3, "São Paulo");
console.log(paises_cidades);
