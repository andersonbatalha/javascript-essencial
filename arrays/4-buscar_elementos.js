const paises = [
    { nome: "Brasil", capital: "Brasília" },
    { nome: "França", capital: "Paris" },
    { nome: "Estados Unidos", capital: "Washington" },
    { nome: "Nova Zelândia", capital: "Wellington" },
    { nome: "Angola", capital: "Luanda" },
    { nome: "Argentina", capital: "Buenos Aires" },
    { nome: "Japão", capital: "Tóquio" },
    { nome: "China", capital: "Pequim" },
    { nome: "Coreia do Sul", capital: "Seul" },
    { nome: "Jamaica", capital: "Kingston" },
    { nome: "Egito", capital: "Cairo" },
    { nome: "Dinamarca", capital: "Copenhague" },
    { nome: "México", capital: "Cidade do México" },
    { nome: "Canadá", capital: "Ottawa" },
    { nome: "Noruega", capital: "Oslo" },
];
console.log(paises.length);

const arrayNumeros = [14,25,7,2,6,0,-1,2,7]

// retorna o valor do primeiro item que satisfaz uma condição
console.log("\nfind(): retorna o primeiro item do array que satisfaz uma condição");
let primeiroPaisComA = paises.find(
    pais => pais.nome.startsWith("A")
);
console.log("Primeiro país que começa com a letra A");
console.log(primeiroPaisComA);

// retorna o índice do primeiro item que satisfaz uma condição
console.log("\nfindIndex(): retorna o índice do primeiro item do array que satisfaz uma condição");
let indice_primeiroPaisComA = paises.findIndex(
    pais => pais.nome.startsWith("A")
);
console.log(`Índice do primeiro país que começa com a letra A: ${indice_primeiroPaisComA}`);

// retorna todos os itens que satisfazem uma condição
console.log("\nfilter(): retorna os itens da lista que satisfazem uma condição");
let filtro = paises.filter(
    pais => pais.capital.startsWith("W")
);
console.log("Capitais que começam com a letra W:");
console.log(filtro);

console.log("\nindexOf(): retorna a primeira ocorrência de um valor no array");
let primeiroIndex = arrayNumeros.indexOf(7);
console.log(`Primeiro index do valor 7: ${primeiroIndex}`);

console.log("\nlastIndexOf(): retorna a última ocorrência de valor no array");
let ultimoIndex = arrayNumeros.lastIndexOf(7);
console.log(`Último index do valor 7: ${ultimoIndex}`);

console.log("\nincludes(): verifica se um valor faz parte de um array");
let umTemNaLista = arrayNumeros.includes(1);
let seteTemNaLista = arrayNumeros.includes(7);
let doisTemNaLista = arrayNumeros.includes(2);
console.log(`1 está no array: ${umTemNaLista}`); // false
console.log(`7 está no array: ${seteTemNaLista}`); // true
console.log(`2 está no array: ${doisTemNaLista}`); // true

console.log("\nsome(): verifica se pelo menos um item da lista satisfaz a condição");
let condicao1 = paises.some(
    pais => pais.nome.startsWith("E")
);
let condicao2 = paises.some(
    pais => pais.nome.endsWith("s")
);
console.log(`algum país começa com a letra E: ${condicao1}`);
console.log(`algum país termina com a letra s: ${condicao2}`);

console.log("\nevery(): verifica se pelo todos os itens da lista satisfazem a condição");
let condicao3 = paises.every(
    pais => pais.capital.endsWith("a")
);
console.log(`todos os países começam com a letra a: ${condicao3}`);