// Criação de arrays

// Método 1
let myList = [0,1,2,3,4,5,6,7,8,9];
console.log(myList);

// Método 2 (cria um array com posições vazias)
let myEmptyList = Array(5);
console.log(myEmptyList);

// Método 3
let cidades = new Array("São Paulo", "Rio de Janeiro", "Brasília", "Manaus", "Belo Horizonte", "Porto Alegre", "Florianópolis", "Curitiba");
console.log(cidades);

// Método 4
let pessoas = Array.of("Maria", "Luisa", "Carlos", "Paulo");
console.log(pessoas);

// Array.of(): executar no console do navegador
/**
 * const tagsUL = document.querySelectorAll('ul');
 * console.log(typeof tagsUL); // NodeList
 * const tagsUL_list = Array.of(tagsUL);
 * console.log(typeof tagsUL_list); // Array
 */

