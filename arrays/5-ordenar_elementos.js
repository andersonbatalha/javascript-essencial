let numeros = [99, 12, 87, 10, 66, 23, 37, 45, 53, 77];

console.log("\nArray original");
console.log(numeros.toString());

//sort
let arrayOrdenado = numeros.sort();
console.log("\nArray ordenado");
console.log(arrayOrdenado.toString());

//reverse
let arrayInverso = arrayOrdenado.reverse();
console.log("\nArray na ordem inversa");
console.log(arrayInverso.toString());
