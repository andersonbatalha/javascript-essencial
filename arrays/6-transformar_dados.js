const alunos = [
    {nome: "Pedro", nota: 10},
    {nome: "Marcos", nota: 8},
    {nome: "Juliana", nota: 5.5},
    {nome: "Mariana", nota: 7.25},
    {nome: "Paula", nota: 4},
];

// join
let alunos_list = alunos.map(
    (aluno) => `${aluno.nome}: Nota ${aluno.nota}`
).join(' | ');
console.log(alunos_list);

// reduce
let media_notas = alunos.reduce(
    (totalNotas, aluno) => totalNotas += aluno.nota, 0
) / alunos.length;
console.log(`Total das notas: ${media_notas}`);

let alunos_list2 = alunos.reduce(
    (string, aluno) => string += `${aluno.nome}: Nota ${aluno.nota} | `, ""
);
console.log(alunos_list2);