// forEach: percorre o array

console.log("\nforEach");
let nomes = ["Carlos", "Paulo", "Marcos",]
nomes.forEach(
    (value, index, array) => {
        console.log(`${array} --> ${index} (${value})`);
    }
);

// Array.map(): permite iterar e modificar os elementos de um array
console.log("\nArray.map()");
let sobrenomes = [" Pereira", " da Silva", " Gomes"]

function adicionarSobrenome(nome, sobrenome) {
    return nome + sobrenome
}
let nome_completo = nomes.map(
    (nome) => adicionarSobrenome(nome, " Silva")
);
console.log(nome_completo);

// Array.flat(): permite modificar os elementos do array de acordo com seu nível de profundidade (depth)
console.log("\nArray.flat()");

let numeros = [4,96, [129, -8, [66, -132]]];
console.info(numeros.flat(2));

// Array.flatMap(): junção das funções map e flat
console.log("\nArray.flatMap()");

let numeros_ = [4, 96, 129, -8, 66, -132];

console.info(numeros_.flatMap(
    (valor) => [[valor * 2]]
));


let frutas = ["Pêra", "Uva", "Maçã", "Laranja", "Banana"];
// Array.keys(): retorna um iterator com as chaves do array; permite percorrer os elementos de uma lista pelo método next()
console.log("\nArray.keys()");
let frutasIteratorKeys = frutas.keys();
console.log(frutasIteratorKeys.next());
console.log(frutasIteratorKeys.next());
console.log(frutasIteratorKeys.next());
console.log(frutasIteratorKeys.next());
console.log(frutasIteratorKeys.next());
console.log(frutasIteratorKeys.next());

// Array.values(): retorna um iterator com os valores do array; permite percorrer os elementos de uma lista pelo método next()
console.log("\nArray.values()");
let frutasIteratorValues = frutas.values();
console.log(frutasIteratorValues.next());
console.log(frutasIteratorValues.next());
console.log(frutasIteratorValues.next());
console.log(frutasIteratorValues.next());
console.log(frutasIteratorValues.next());
console.log(frutasIteratorValues.next());

// Array.entries(): retorna um iterator com um par (chave, valor) do array; permite percorrer os elementos de uma lista pelo método next()
console.log("\nArray.entries()");
let frutasIteratorEntries = frutas.entries();
console.log(frutasIteratorEntries.next());
console.log(frutasIteratorEntries.next());
console.log(frutasIteratorEntries.next());
console.log(frutasIteratorEntries.next());
console.log(frutasIteratorEntries.next());
console.log(frutasIteratorEntries.next());