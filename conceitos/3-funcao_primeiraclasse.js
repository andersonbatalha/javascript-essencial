// Funções de primeira classe

function mensagem(texto) {
    console.log(texto);
}

//1 - atribuir o valor de uma variável a uma função
var minha_funcao = mensagem("Olá mundo! (1)");
var minha_funcao2 = mensagem;
var funcao_soma = function soma(x,y) { console.log(x+y); }

// 2 - passar uma função como parâmetro para outra função
function executar(funcao) { // recebe uma função como parâmetro
    return funcao;    
}

executar(minha_funcao);
executar(minha_funcao2("Olá mundo! (2)"));

// 3 - Atribuir uma função a uma estrutura de dados (lista ou objeto)

lista_funcoes = [mensagem, executar];

lista_funcoes[0]("Teste (3)");
lista_funcoes[1](funcao_soma(3,5));

funcoes = {
    'funcao1': mensagem,
    'funcao2': executar,
}

funcoes.funcao1("Olá mundo (4)");
funcoes.funcao2(funcao_soma(18, 39));