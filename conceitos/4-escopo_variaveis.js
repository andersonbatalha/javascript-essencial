/* 
Tipos de escopo 
*/

var nome = "Anderson"; // escopo global
let idade; // escopo global
console.log(`Variável global idade: ${idade}`); // undefined

if (nome == "Anderson") {
    let idade = 25; // escopo de bloco
    console.log(`Variável idade dentro do if: ${idade}`);
}

function imprimeIdade() {
    let idade = 12; // escopo de função
    console.log(`Variável idade dentro da função: ${idade}`);
}
imprimeIdade()

