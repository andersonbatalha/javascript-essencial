// Imutabilidade

const pessoa = {
    'nome': "Anderson",
    'sobrenome': "Pontes Batalha",
}

function nomeCompleto(obj) {
    return {
        ...obj, // spread
        'nome_completo': `${obj.nome} ${obj.sobrenome}`
    }
}

const func = nomeCompleto(pessoa);

console.log(func, "\n",pessoa);

const students = [
    {
        'name': "Grace",
        'grade': 10
    },
    {
        'name': "John",
        'grade': 5
    },
    {
        'name': "Peter",
        'grade': 8
    },
]

function getApprovedStudents(studentsList) {
    return studentsList.filter(student => student.grade >= 7);
}

const approved_students = getApprovedStudents(students);

console.log(approved_students);
console.log(students);