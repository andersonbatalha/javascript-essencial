// Exemplo de tipagem fraca

var meu_nome = "Anderson";
var idade = 25;
var lista = [1,2,3,4,5]

console.log(meu_nome + idade) // Anderson25
console.log(lista+meu_nome) // 1,2,3,4,5Anderson

/* 
Em linguagens de tipagem forte (como Python, por exemplo), não é possível realizar operações com tipos de dados diferentes
*/