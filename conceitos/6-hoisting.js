// Hoisting

// Hoisting de variável
function teste() {
    var texto;

    console.log(texto); // undefined

    texto = "Exemplo";

    console.log(texto); // Exemplo
}

teste();

// Hoisting de função

function teste2() {
    
    mensagem("Teste");

    function mensagem(texto) {
        console.log(texto);
    }

}

teste2();