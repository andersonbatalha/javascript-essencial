// Currying: consiste em transformar uma função com vários parâmetros em uma função com apenas um.

function multiplicacao(x,y) {
    return x*y;
}

console.log(multiplicacao(4,2));
console.log(multiplicacao(4,3));
console.log(multiplicacao(4,4));
console.log(multiplicacao(4,5));


console.log("\n\n\n\n");

function mult(primeiro_termo) {
    return function (segundo_termo) {
        return primeiro_termo * segundo_termo;
    }
}

const funcao1 = mult(4); // evita a repetição do primeiro termo
console.log(funcao1(2));
console.log(funcao1(3));
console.log(funcao1(4));
console.log(funcao1(5));
