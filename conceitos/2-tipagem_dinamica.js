// Exemplo de tipagem dinâmica

var x = 25;
console.log(x);

x = "Teste";
console.log(x);

/* 
Em linguagens de tipagem dinâmica, é possível reatribuir o tipo de variável após sua criação
Em linguagens de tipagem estática (como Java, por exemplo), não é possível fazer isso 
*/