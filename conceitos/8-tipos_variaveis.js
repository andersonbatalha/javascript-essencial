// var, let, const

var texto1 = "Exemplo"; // global

console.log("var");
function funcao_teste1() {
    var texto1 = "Exemplo (função)";
    console.log(`Valor da variável texto1 na função: ${texto1}`);

    if (true) {
        var texto1 = "Exemplo 1 (if)";
        console.log(`Valor da variável dentro do if: ${texto1}`);
    }

    console.log(`Valor da variável após o if na função: ${texto1}`);
}

funcao_teste1();

console.log("\nlet");
function funcao_teste2() {
    let texto2 = "Exemplo (função)"; // valor só vale dentro da função
    console.log(`Valor da variável texto2 na função: ${texto2}`);

    if (true) {
        let texto2 = "Exemplo 2 (if)"; // variável criada com let respeita o escopo de bloco
        console.log(`Valor da variável dentro do if: ${texto2}`);
    }

    console.log(`Valor da variável após o if na função: ${texto2}`);
}

funcao_teste2();

console.log("\nconst");
function funcao_teste3() {
    const texto3 = "Exemplo (função)"; // valor só vale dentro da função
    console.log(`Valor da variável texto3 na função: ${texto3}`);

    if (true) {
        const texto3 = "Exemplo 3 (if)"; // variável criada com const respeita o escopo de bloco
        console.log(`Valor da variável dentro do if: ${texto3}`);
    }

    console.log(`Valor da variável após o if na função: ${texto3}`);
}

funcao_teste3();

console.log("\nConstantes");

const nome = "Anderson";
// nome = "Paulo";
/*  Não é possível reatribuir o valor de uma variável criada com 'const'. */

const idade = 25;
// idade = 25  /* Erro! */

const lista = [1,2,3,4,5]
// Em listas é possível adicionar ou remover elementos, mas não atribuir um novo valor à variável

lista.shift(); //2,3,4,5
console.log(lista)

lista.pop() // 2,3,4
console.log(lista)




