/*
if

Sintaxe:

if (condicao) {
    instrucoes
}

*/

console.log("IF");
var idade = 54;

if (idade >= 18) {
    console.log("É maior de idade.");
}

/*
if-else

Sintaxe:

if (condicao) {
    instrucoes
}
else {
    instrucoes
}

*/

console.log("\n\nIF-ELSE");
var saldo = -250;

if (saldo < 0 ) {
    console.log("Está devendo! Pague suas dívidas o quanto antes.");
}
else {
    console.log("É um bom pagador. Continue assim!");
}

/*
if-else if-else

Sintaxe:

if (condicao) {
    instrucoes
}
else if {
    instrucoes
}
else if {
    instrucoes
}
else {
    instrucoes
}

*/

console.log("\n\nIF- ELSE IF - ELSE");
var salario = 3500;
if (salario < 1500) {
    console.log("Pertence a classe baixa");
}
else if (salario > 1500 && salario < 7000) {
    console.log("Pertence a classe média");
}
else {
    console.log("Pertence a classe alta");
}


/*
switch

Sintaxe: 

switch (variavel):
    case valor1:
        instrucoes
        break
    case valor2:
    case valor3:
        instrucoes
        break
    ...
    case valorN;
    default:
        instrucoes
*/

console.log("\n\nSWITCH");
var cor = "verde";
switch (cor) {
    case "vermelho":
        console.log("Pare imediatamente!");
        break;
    case "amarelo":
        console.log("Atenção! O sinal vai fechar.");
        break;
    case "verde":
        console.log("Siga em frente!");
        break;
    default:
        console.log("Não corresponde a nenhuma cor do semáforo.");
        break;
}