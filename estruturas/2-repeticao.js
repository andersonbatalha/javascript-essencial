console.log("FOR");
/** Sintaxe:
 * for (inicio, condicao, incremento) {
 *      instrucoes
 * }
 */
var str = " ";
for (let i = 0; i <= 25; i++) {
    str += i.toString().concat(" ");
}
console.log(str);

console.log("\n\nFOR ... IN");
/** Sintaxe:
 * for (var item in lista) {
 *      instrucoes
 * }
 */
let frutas = ["Pêra", "Maçã", "Uva", "Romã", "Morango", "Mamão", "Melancia", "Jaca", "Manga", "Laranja", "Tangerina", "Mexerica", "Goiaba", "Limão"]
frutas.local_compra1 = "Mercado";
frutas.local_compra2 = "Sacolão";
for (var fruta in frutas) { // itens da lista e propriedades
    console.log(frutas[fruta]);
}
console.log("\n\nFOR ... OF");
for (var fruta of frutas) { // itens da lista 
    console.log(fruta);
}

console.log("WHILE: primeiro verifica se a condição é verdadeira e depois executa as instruções");
let contador = 0;
let limite = 50;
let numeros = [];

while (contador < limite) {
    contador++;
    numeros.push(contador);
}
console.log(numeros);


/** Sintaxe:
 * while (condicao) {
 *      instrucoes
 * }
 */

console.log("\n\nDO ... WHILE: primeiro executa as instruções e depois verifica se a condição é verdadeira");
/** Sintaxe:
 * do {
 *      instrucoes
 * } while (condicao)
 */
let valor = 0;

do {
    console.log(`Iteração #${valor} - Número gerado: ${parseInt(Math.random() * 100)}`);
    valor++;
} while (valor <= 10);

console.log("\n\nCONTINUE");
for (var i = 0; i < 20; i++) {
    if (i % 3 === 0 && i % 6 === 0) {
        continue;
    }
    console.log(i);
}

console.log("\n\nBREAK");
var cont = 0;
while (cont < 15) {
    console.log(cont);
    cont++;
    if (cont === 8) {
        break;
    }
}