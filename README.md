# JavaScript ES6 essencial

[1. História](/docs/1-HISTORIA.md)

[2. Conceitos](docs/2-CONCEITOS.md)

[3. Tipos de dados](docs/3-TIPOS_DADOS.md)

[4. Functions](docs/4-FUNCTIONS.md)

[5. Operadores](docs/5-OPERADORES.md)

[6. Orientação a objetos](docs/6-ORIENTACAO_OBJETOS.md)

[7. Design Patterns](docs/7-DESIGN_PATTERNS.md)

[8. Arrays](docs/8-ARRAYS.md)
