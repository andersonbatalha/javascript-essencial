// Modificadores de acesso

function Pessoa(nome) {
    let nome_ = nome; // variável nome_ é privada

    Object.defineProperty(this, 'name', {
        get: function () {
            return this.nome_;
        },
        set: function (novo_nome) {
            this.nome_ = novo_nome;
        },
    });
}

const p = new Pessoa();
console.log(p);
console.log('\nsetNome()'); p.name = "Anderson";
console.log(`\ngetNome() = ${p.name}`);
