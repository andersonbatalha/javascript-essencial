// Modificadores de acesso

function Pessoa(nome) {
    let nome_ = nome; // variável nome_ é privada

    this.setNome = (nome_pessoa) => {
        nome_ = nome_pessoa;
    }

    this.getNome = () => {
        return nome_;
    }
}

const p = new Pessoa();
console.log(p);
console.log(`p.nome_ = ${p.nome_}`); // undefined
console.log('\nsetNome()');
p.setNome("Teste");
console.log(`\ngetNome() = ${p.getNome()}`);
