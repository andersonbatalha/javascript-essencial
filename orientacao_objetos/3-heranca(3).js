function Animal(qtdePatas) { }

// é possível definir atributos e métodos no prototype da função
Animal.prototype.qtdePatas = 0;
Animal.prototype.movimentar = () => {
    console.log("Andando...");
}

function Cachorro(morde) {
    this.qtdePatas = 4;
    this.morde = morde;
}
Cachorro.prototype = Object.create(Animal);
Cachorro.prototype.latir = () => {
    console.log("Au! Au! Au!");
} 

console.log("\nPinscher");
const pinscher = new Cachorro(true);
pinscher.latir();

console.log("\nPug");
const pug = new Cachorro(false);
pug.latir()