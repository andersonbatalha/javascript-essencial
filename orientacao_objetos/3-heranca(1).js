function Animal(qtdePatas) {
    this.qtdePatas = qtdePatas;
}

function Cachorro(morde) {
    Animal.call(this, 4); // passa como contexto para a função Animal o valor de 4 para atributo qtdePatas

    this.morde = morde;

}

const pinscher = new Cachorro(true);
console.log(`\nNúmero de patas (pinscher) = ${pinscher.qtdePatas}`); // 4
console.log(`Morde (pinscher) = ${pinscher.morde}`); // true

const pug = new Cachorro(false);
console.log(`\nNúmero de patas (pug) = ${pug.qtdePatas}`); // 4
console.log(`Morde (pug) = ${pug.morde}`); // false
