const myText = "Texto exemplo";
console.log(myText.split(' '));

console.log("\n__proto__: se refere ao objeto da classe String")
console.log(`myText.__proto__ ${myText.__proto__.split}`);

console.log("\nprototype: se refere a classe String"); 
const myText2 = String("Outro texto exemplo");
console.log(`String.prototype: ${String.prototype.split}`);

console.log(`\nString.prototype === myText.__proto__ -> ${String.prototype === myText.__proto__}`);

console.log(`\nConstructor da variável myText2 é a classe String`);
console.log(`myText2.constructor === String: ${myText2.constructor === String}`);

console.log(`\nmyText2.constructor === String`);
console.log(`myText2.__proto__ === String.prototype`);








