// Modificadores de acesso

class Pessoa {
    #name = ''; // Utilizar o símbolo # para definir uma variável privada
    constructor(nome) {
        this.#name = nome;
    }

    getNome() {
        return this.#name;
    }

    setNome(novo_nome) {
        this.#name = novo_nome;
    }
}
const p = new Pessoa();
console.log(p);
console.log(`p.nome_ = ${p.nome_}`); // undefined
console.log('\nsetNome()'); p.setNome("Anderson");
console.log(`\ngetNome() = ${p.getNome()}`);
