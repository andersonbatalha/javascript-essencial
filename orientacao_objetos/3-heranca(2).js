function Animal(qtdePatas) {
    this.qtdePatas = qtdePatas;

    this.movimentar = function () {
        console.log("Andando...");
    }
}

function Cachorro(morde) {
    Animal.call(this, 4); // passa como contexto para a função Animal o valor de 4 para atributo qtdePatas

    this.morde = morde;

    this.latir = () => {
        console.log("Au! Au! Au!");
    }

}

console.log("\nPinscher");
const pinscher = new Cachorro(true);
pinscher.movimentar(); // Cachorro herda movimentar() de Animal
pinscher.latir();

console.log("\nPug");
const pug = new Cachorro(false);
pug.movimentar() // Cachorro herda movimentar() de Animal
pug.latir()