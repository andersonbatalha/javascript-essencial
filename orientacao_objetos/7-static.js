function Person() {

}

Person.falar = function ola() {
    return "Olá mundo";
}

console.log(Person.falar());

// JavaScript ES6

class PersonClass {
    static falar(frase) {
        return frase;
    }
}

console.log(PersonClass.falar("Olá, e aí galera?"));