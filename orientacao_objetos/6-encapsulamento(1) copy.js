// Modificadores de acesso

class Pessoa {
    #name = '';
    constructor(nome) {
        this.#name = nome;
    }

    get nome() {
        return this.#name;
    }

    set nome(novo_nome) {
        this.#name = novo_nome;
    }
}
const p = new Pessoa();
console.log(p);

console.log('\nsetNome()');
p.nome = "Anderson";
console.log(`\ngetNome() = ${p.nome}`);
