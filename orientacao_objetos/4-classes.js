// Sintaxe de classe JavaScript ES6

class Animal {
    constructor(qtdePatas) {
        this.qtdePatas = qtdePatas;
    }
    movimentar() {
        console.log("Andando...");
    }
}

class Cachorro extends Animal {
    constructor(morde) {
        super(4);
        this.morde = morde;
    }

    latir() {
        console.log("Au! Au! Au!");
    }
}

const cachorro = new Cachorro(true);
console.log(cachorro);
cachorro.latir();