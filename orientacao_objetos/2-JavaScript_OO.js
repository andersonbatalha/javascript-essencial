function Animal() {
    this.qtdePatas = 4;
}

let cachorro = new Animal();
console.log("Valor do atributo qtdePatas = " + cachorro.qtdePatas); // 4
console.log("\nO __proto__ se refere a instância da classe (cachorro). Já o prototype está se referindo a classe (Animal).");
console.log(`cachorro.__proto__ === Animal.prototype -> ${cachorro.__proto__ === Animal.prototype}`);
console.log(`Animal.__proto__ === Function.prototype -> ${Animal.__proto__ === Function.prototype}`);

console.log("\nInstance of");
console.log(`cachorro instanceof Animal -> ${cachorro instanceof Animal}`); // true
console.log(`cachorro instanceof Function -> ${cachorro instanceof Function}`); // false
console.log(`Animal instanceof Function -> ${Animal instanceof Function}`); // true


/**
 * O que ocorre ao criar um objeto 
 * 
 * new Animal()
 * 
 * Um novo objeto é criado
 * A função construtora é chamada com os argumentos especificados e com o this vinculado ao novo objeto
 * Caso a função construtora tenha um retorno explícito, será executado seu `return`. Caso contrário, será retornado o objeto
 */

function Carro(marca, modelo) {
    this.marca = marca;
    this.modelo = modelo;
    
    return {
        marca: "VW",
        modelo: "Gol"
    };
} 

const c = new Carro("Chevrolet", "Prisma");
console.log("\n");
console.log(c); // a classe retorna o que está no bloco return definido na função, não os argumentos passados